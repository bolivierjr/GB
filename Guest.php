<?php

require 'inc/dbConnect.php';

class Guest
{
    private $name;
    private $email;
    private $dbh;

    public function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    public function create($params)
    {
        $sql = 'INSERT INTO tbl_users (name, email)
                        VALUES(:name, :email)';

        $stmt = $this->$dbh->prepare($sql);
        $stmt->execute($params);
    }

    public function read()
    {
        $sql = 'SELECT name,email FROM tbl_users';

        $stmt = $this->dbh->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetchAll();
    }

    public function delete()
    {

    }

}
$params = [
    ':name' => $_POST['name'],
    ':email' => $_POST['email']
];

$newGeust = new Guest($dbh);
$newGuest->create($params);
