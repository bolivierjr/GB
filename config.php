<?php


return [
   'dsn' => 'mysql:host=localhost;dbname=guestbook;port=33060',
   'user' => 'homestead',
   'pass' => 'secret',
   'opt' => array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
];
