<?php

ini_set("display_errors", "1");
error_reporting(E_ALL);
$config = require('config.php');

try{
    $dbh = new PDO(
        $config['dsn'],
        $config['user'],
        $config['pass'],
        $config['opt']
      );

    $sql = 'SELECT id,name FROM tbl_users';

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

    foreach($results as $result) {
         echo '<li style="list-style-type:none;">'.$result['name'];
         echo '<form class="form-group" action="delete.php" method="POST">';
         echo '<input type=hidden name="row-id" value="'.$result['id'].'">';
         echo '<input type=submit value="Delete">';
         echo '</form></li>';
         
    }
} catch(PDOException $e) {
    echo "Error: ".$e->getMessage();
    exit();
} finally {
    $dbh = null;

}
