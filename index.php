<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Guestbook Page</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="mx-auto">
          <h1>My Guestbook Page</h1>

          <p>Please fill out your info to be added to the guestbook:</p>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="mx-auto">

          <form class="form-group" action="addguest.php" method="POST">
            <label for="name">Full Name</label>
            <br>
            <input class="form-control" id="name" type="text" name="name" maxlength="30" placerholder="name" required>
            <br>
            <label for="email">Email</label>
            <br>
            <input class="form-control" id="email" type="email" name="email" maxlength="30" placerholder="name" required>
            <?php
                session_start();
                $error = $_SESSION['error'];
                if(!empty($error)) {
                  echo $error;
                }

                session_destroy();
                ?>
            <br>
            <input class="btn btn-primary float-right" type="submit" value="Submit">
          </form>
          <br>
          <table>
              <tr>
                <td>
                  <ul>

                    <?php
                        require 'listguests.php';
                    ?>

                  </ul>
                </td>
              </tr>
          </table>

        </div>
      </div>
    </div>

  </body>
</html>
