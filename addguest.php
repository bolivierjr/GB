<?php
ini_set("display_errors", "1");
error_reporting(E_ALL);

$config = require 'config.php';


try {
    $dbh = new PDO(
        $config['dsn'],
        $config['user'],
        $config['pass'],
        $config['opt']
      );

    $params = [
      $_POST['name'],
      $_POST['email'],
    ];

    $sql = 'INSERT INTO tbl_users (name, email)
                    VALUES(?, ?)';

    $stmt = $dbh->prepare($sql);
    $stmt->execute($params);

    header('Location: http://homestead.test/');

} catch(PDOException $e) {
    session_start();
    $_SESSION['error'] = 'This email has been used already.';
    header('Location: http://homestead.test/');
    
} catch(Exception | Error $e) {
    echo 'Error: '.$e->getMessage();

} finally {
    $dbh = null;
}
