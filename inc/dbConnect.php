<?php


try {
    $dbh = new PDO(
        $config['dsn'],
        $config['user'],
        $config['pass'],
        $config['opt']
      );

} catch(PDOException $e) {
    echo 'Error: '.$e->getMessage();


} finally {
    $dbh = null;

}
