<?php

ini_set("display_errors", "1");
error_reporting(E_ALL);
$config = require('config.php');

try{
    $dbh = new PDO(
        $config['dsn'],
        $config['user'],
        $config['pass'],
        $config['opt']
      );

    $params = [
      'id' => $_POST['row-id'],
    ];
    
    $sql = 'DELETE FROM tbl_users WHERE id=?';

    $stmt = $dbh->prepare($sql);
    $stmt->execute(array($_POST['row-id']));

    header('Location: http://homestead.test/');

  } catch(PDOException $e) {
  echo "Error: ".$e->getMessage();
  exit();

  } finally {
  $dbh = null;

  }
